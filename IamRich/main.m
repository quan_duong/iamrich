//
//  main.m
//  IamRich
//
//  Created by Duong Tien Quan on 11/5/15.
//  Copyright © 2015 vtvcab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
